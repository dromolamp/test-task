// Mock db as Array
const users = [];

export class UserService {
    async getCurrentUser() {
        const [user] = users.slice(-1);

        return user || null;
    }

    async saveUser(newUser) {
        users.push(newUser);

        return newUser;
    }
}
