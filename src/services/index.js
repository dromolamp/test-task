import { UserService } from './user.service.js';
import { BoredapiService } from './boredapi.service.js';
import { ActivityService } from './activity.service.js';

export const userService = new UserService();
export const boredapiService = new BoredapiService();
export const activityService = new ActivityService();

export default {
    userService,
    boredapiService,
    activityService,
}
