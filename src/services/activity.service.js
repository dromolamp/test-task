import activityPriceLabels from '../constants/activityPriceLabels.js';
import activityAccessibilityLabels from '../constants/activityAccessibilityLabels.js';
import { BoredapiActivityParams } from '../utils/BoredapiActivityParams.js';

export class ActivityService {
    getFilterParamsByUser(user) {
        if (!user) return {};

        const { price, accessibility } = user;
        const priceRange = price ? this.getPriceRanges()[price] : null;
        const accessibilityRange = accessibility ? this.getAccessibilityRanges()[accessibility] : null;

        return new BoredapiActivityParams(accessibilityRange, priceRange)
    }

    getPriceRangeByValue(value) {
        return this.getRangeByValue(this.getPriceRanges(), value);
    }

    getAccessibilityRangeByValue(value) {
        return this.getRangeByValue(this.getAccessibilityRanges(), value);
    }

    getPriceLabelByValue(value) {
        return this.getRangeKey(this.getPriceRanges(), value);
    }

    getAccessibilityLabelByValue(value) {
        return this.getRangeKey(this.getAccessibilityRanges(), value);
    }

    getPriceRanges () {
        return {
            [activityPriceLabels.FREE]: { max: 0 },
            [activityPriceLabels.LOW]: { min: 0, max: 0.5 },
            [activityPriceLabels.HIGH]: { min: 0.5 },
        }
    }

    getAccessibilityRanges () {
        return {
            [activityAccessibilityLabels.HIGH]: { max: 0.25 },
            [activityAccessibilityLabels.MEDIUM]: { min: 0.25, max: 0.75 },
            [activityAccessibilityLabels.LOW]: { min: 0.75 },
        }
    }

    getRangeKey(range, number) {
        return Object.keys(range).find((key) => {
            const { min, max } = range[key];
            return (min === undefined || number > min) && (max === undefined || number <= max);
        });
    }

    getRangeByValue(ranges, value) {
        const rangeKey = this.getRangeKey(ranges, value);
        return ranges[rangeKey];
    }
}
