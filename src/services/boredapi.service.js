import axios from 'axios';

export class BoredapiService {
    baseUrl = 'https://www.boredapi.com';

    async getRandomActivity(params = {}) {
        try {
            const { data, status } = await axios.get(`${this.baseUrl}/api/activity`, { params });
            return status === 200 ? data : null;
        } catch (error) {
            console.error('Error fetching activity:', error.message);
            return null;
        }
    }
}
