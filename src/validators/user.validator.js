import Joi from 'joi';

import priceLabels from '../constants/activityPriceLabels.js';
import accessibilityLabels from '../constants/activityAccessibilityLabels.js';

const createUser = Joi.object({
    name: Joi.string().required(),
    accessibility: Joi.string().valid(...Object.values(accessibilityLabels)),
    price: Joi.string().valid(...Object.values(priceLabels)),
});

export default {
    createUser,
}
