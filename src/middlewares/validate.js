import httpStatus from 'http-status';

import { ApiError } from '../utils/ApiError.js';

const validate = (schema) => (req, res, next) => {
    const object = req.body || {};
    const { value, error } = schema
        .prefs({ errors: { label: 'key' }, abortEarly: false })
        .validate(object);

    if (error) {
        const errorMessage = error.details.map((details) => details.message).join(', ');

        return next(new ApiError(httpStatus.BAD_REQUEST, errorMessage));
    }
    Object.assign(req, { body: value });
    return next();
};

export default validate;
