import { userService } from '../services/index.js';

const auth = () => async (req, res, next) => {
    try {
        req.user = await userService.getCurrentUser();
        return next();
    } catch (e) {
        return next(err);
    }
};

export default auth;
