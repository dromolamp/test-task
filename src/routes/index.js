import { Router } from 'express';
import httpStatus from 'http-status';

import userRoute from './user.route.js';
import activityRoute from './activity.route.js';
import { ApiError } from '../utils/ApiError.js';

const router = Router();

router.use('/user', userRoute);
router.use('/activity', activityRoute);

// 404 handler
router.use((req, res, next) => {
    next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

export default router;
