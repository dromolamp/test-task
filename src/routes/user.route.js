import { Router } from 'express';

import auth from '../middlewares/auth.js';
import validate from '../middlewares/validate.js';
import userController from '../controllers/user.controller.js';
import userValidator from '../validators/user.validator.js';

const router = Router();

router
    .route('/')
    .post(validate(userValidator.createUser), userController.createUser)
    .get(auth(), userController.getUser);

export default router;
