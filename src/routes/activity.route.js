import { Router } from 'express';

import activityController from '../controllers/activity.controller.js';
import auth from '../middlewares/auth.js';

const router = Router();

router
    .route('/')
    .get(auth(), activityController.getActivity);

export default router;
