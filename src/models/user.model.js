export class User {
    constructor(name, accessibility, price) {
        Object.assign(this, { name, accessibility, price });
    }
}
