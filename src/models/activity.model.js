import { activityService } from '../services/index.js'

export class Activity {
    constructor(activity, accessibility, type, participants, price, link, key) {
        Object.assign(this, { activity, accessibility, type, participants, price, link, key });
    }

    mapAccessibilityLevel = () => activityService.getAccessibilityLabelByValue(this.accessibility);

    mapPriceLevel = () => activityService.getPriceLabelByValue(this.price);

    toJSON() {
        return {
            activity: this.activity,
            accessibility: this.mapAccessibilityLevel(),
            type: this.type,
            participants: this.participants,
            price: this.mapPriceLevel(),
            link: this.link,
            key: this.key
        };
    }
}
