export class BoredapiActivityParams {
    minaccessibility;
    maxaccessibility;
    minprice;
    maxprice;

    constructor(accessibility, price) {
        this.setRange(price, 'price');
        this.setRange(accessibility, 'accessibility');
    }

    setRange(value, attributeName) {
        if (value) {
            const { min, max } = value;
            if(min >= 0) {
                // min is always > (not >=)
                // but boreadpi uses ">= MIN_VALUE" with "="
                // We need to add step value (for example 0.001, api uses 0.01 but 0.001 works fine too) for min value to have ">" case.
                this[`min${attributeName}`] = min + 0.001;
            }
            if(max >= 0) {
                // max is always "<=" for us and for API.
                this[`max${attributeName}`] = max;
            }
        }
    }

    toJSON() {
        return {
            minaccessibility,
            maxaccessibility,
            minprice,
            maxprice,
        };
    }
}
