import express from 'express';
import bodyParser from 'body-parser';

import routes from './routes/index.js';
import { errorConverter, errorHandler } from './middlewares/error.js';

const app = express();

app.use(bodyParser.json());

app.use(routes);

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

const port = 3000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
