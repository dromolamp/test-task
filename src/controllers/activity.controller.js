import { Activity } from '../models/activity.model.js';
import { boredapiService, activityService } from '../services/index.js';

class ActivityController {
    async getActivity(req, res) {
        const { user } = req;
        const activityRequestParams = activityService.getFilterParamsByUser(user);

        const data = await boredapiService.getRandomActivity(activityRequestParams);

        if(!data || !data.key) {
            return res.status(404).json({ error: 'Activity not found' });
        }

        const { accessibility, price, activity, type, participants, link, key } = data;

        const activityModel = new Activity(
            activity,
            accessibility,
            type,
            participants,
            price,
            link,
            key
        );

        return res.json(activityModel);
    }
}

export default new ActivityController();
