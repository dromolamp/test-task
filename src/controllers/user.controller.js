import { userService } from '../services/index.js';
import { User } from '../models/user.model.js';

class UserController {
    async createUser(req, res) {
        const { name, accessibility, price } = req.body || {};

        const newUser = new User(name, accessibility, price);
        const user = await userService.saveUser(newUser);

        await res.json({ user });
    }

    getUser(req, res) {
        const { user } = req;

        res.json({ user });
    }
}

export default new UserController();
